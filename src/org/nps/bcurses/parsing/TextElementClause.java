package org.nps.bcurses.parsing;

import org.nps.bcurses.data.TextElement;
import org.nps.bcurses.io.TextInput;

import java.util.function.Function;

public class TextElementClause {

    final CharSequence start;
    final Function<TextInput, TextElement> parse;

    public TextElementClause(CharSequence start, Function<TextInput, TextElement> parse) {
        this.start = start;
        this.parse = parse;
    }

    public CharSequence getStart() {
        return start;
    }

    public TextElement parse(TextInput str) {
        return parse.apply(str);
    }

}
