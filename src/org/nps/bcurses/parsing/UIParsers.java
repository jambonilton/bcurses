package org.nps.bcurses.parsing;

import org.nps.bcurses.data.LeafTextElement;
import org.nps.bcurses.data.TextElement;
import org.nps.bcurses.data.TextType;
import org.nps.bcurses.io.TextInput;
import org.nps.bcurses.script.ScriptExecutor;
import org.nps.bcurses.ui.UIParser;

import java.util.function.Function;

public class UIParsers {

    public static UIParser markdownParser() {
        return new GenericUIParser(
            clause("*", MarkdownSyntax::readEmph),
            clause("[", MarkdownSyntax::readLink)
        );
    }

    public static UIParser markdownParserWithScripting(ScriptExecutor engine) {
        return new GenericUIParser(
            clause("*", MarkdownSyntax::readEmph),
            clause("[", MarkdownSyntax::readLink),
            clause("${", new ParseScript(engine))
        );
    }

    static TextElementClause clause(CharSequence str, Function<TextInput, TextElement> parse) {
        return new TextElementClause(str, parse);
    }

    static class ParseScript implements Function<TextInput, TextElement> {

        final ScriptExecutor engine;

        public ParseScript(ScriptExecutor engine) {
            this.engine = engine;
        }

        @Override
        public TextElement apply(TextInput in) {
            final String script = getScript(in);
            final Object value = engine.execute(script);
            return new LeafTextElement(String.valueOf(value), TextType.EMPHASIZED);
        }

        private String getScript(TextInput in) {
            final StringBuilder sb = new StringBuilder();
            in.skip("${");
            while (!in.isNext("}"))
                sb.append(in.next());
            in.skip("}");
            return sb.toString();
        }

    }

}
