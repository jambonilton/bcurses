package org.nps.bcurses.parsing;

import org.nps.bcurses.data.LeafTextElement;
import org.nps.bcurses.data.LinkElement;
import org.nps.bcurses.data.TextElement;
import org.nps.bcurses.io.TextInput;

import static org.nps.bcurses.data.TextType.EMPHASIZED;

public class MarkdownSyntax {

    static TextElement readEmph(TextInput chars) {
        chars.skip("*"); // burn the *
        final StringBuilder sb = new StringBuilder();
        while (chars.hasNext() && chars.peek().charValue() != '*')
            sb.append(chars.next());
        chars.skip("*"); // burn the *
        return new LeafTextElement(sb.toString(), EMPHASIZED);
    }

    static TextElement readLink(TextInput chars) {
        chars.skip("["); // burn the [
        final StringBuilder label = new StringBuilder(), link = new StringBuilder();
        while (chars.hasNext() && chars.peek().charValue() != ']')
            label.append(chars.next());
        chars.skip("](");
        while (chars.hasNext() && chars.peek().charValue() != ')')
            link.append(chars.next());
        chars.skip(")");
        return new LinkElement(label.toString(), link.toString());
    }

}
