package org.nps.bcurses.parsing;

import org.nps.bcurses.data.TextBuilder;
import org.nps.bcurses.io.TextInput;
import org.nps.bcurses.ui.TextUINode;
import org.nps.bcurses.ui.UINode;
import org.nps.bcurses.ui.UIParser;

import java.util.Optional;

import static java.util.Arrays.asList;

public class GenericUIParser implements UIParser {

    final Iterable<TextElementClause> clauses;

    public GenericUIParser(TextElementClause... clauses) {
        this(asList(clauses));
    }

    public GenericUIParser(Iterable<TextElementClause> clauses) {
        this.clauses = clauses;
    }

    @Override
    public UINode read(TextInput str) {
        final TextBuilder builder = new TextBuilder();

        StringBuilder sb = new StringBuilder();
        while (str.hasNext()) {
            final Optional<TextElementClause> clause = matchClause(str);
            if (clause.isPresent()) {
                sb = append(builder, sb);
                builder.add(clause.get().parse(str));
            } else {
                sb.append(str.next());
            }
        }
        append(builder, sb);

        return new TextUINode(builder.build());
    }

    private StringBuilder append(TextBuilder builder, StringBuilder sb) {
        if (sb.length() > 0) {
            builder.add(sb.toString());
            return new StringBuilder();
        }
        return sb;
    }

    private Optional<TextElementClause> matchClause(TextInput str) {
        for (TextElementClause clause : clauses)
            if (str.isNext(clause.getStart()))
                return Optional.of(clause);
        return Optional.empty();
    }

}
