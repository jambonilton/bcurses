package org.nps.bcurses.data;

import java.util.Iterator;
import java.util.List;

public class ParentTextElement implements TextElement {

    final TextType type;
    final List<TextElement> children;

    public ParentTextElement(TextType type, List<TextElement> children) {
        this.type = type;
        this.children = children;
    }

    @Override
    public String getText() {
        return "";
    }

    @Override
    public TextType getType() {
        return type;
    }

    @Override
    public Iterator<TextElement> iterator() {
        return children.iterator();
    }

}
