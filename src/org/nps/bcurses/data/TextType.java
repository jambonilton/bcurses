package org.nps.bcurses.data;

public enum TextType {
    ROOT,
    NORMAL,
    PARAGRAPH,
    EMPHASIZED,
    SCRIPT,
    LINK
}
