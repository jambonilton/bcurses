package org.nps.bcurses.data;

public interface TextElement extends Iterable<TextElement> {

    String getText();

    TextType getType();

    default boolean is(TextType attribute) {
        return getType().equals(attribute);
    }

    default boolean hasChildren() {
        return iterator().hasNext();
    }

}
