package org.nps.bcurses.data;

import org.nps.bcurses.ui.UIOption;

public class LinkElement extends LeafTextElement implements UIOption {

    final String reference;

    public LinkElement(String text, String reference) {
        super(text, TextType.LINK);
        this.reference = reference;
    }

    @Override
    public String getScript() {
        return reference;
    }

}
