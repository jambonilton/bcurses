package org.nps.bcurses.data;

public class ScriptTextElement extends LeafTextElement {

    public ScriptTextElement(String text) {
        super(text, TextType.SCRIPT);
    }

}
