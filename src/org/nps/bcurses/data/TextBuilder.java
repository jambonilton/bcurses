package org.nps.bcurses.data;

import java.util.ArrayList;
import java.util.List;

public class TextBuilder {

    final List<TextElement> elements;

    public TextBuilder() {
        this(new ArrayList<>());
    }

    public TextBuilder(List<TextElement> elements) {
        this.elements = elements;
    }

    public TextBuilder add(String text) {
        return add(new LeafTextElement(text, TextType.NORMAL));
    }

    public TextBuilder add(String text, TextType type) {
        return add(new LeafTextElement(text, type));
    }

    public TextBuilder add(TextElement element) {
        this.elements.add(element);
        return this;
    }

    public TextElement build() {
        return new ParentTextElement(TextType.ROOT, elements);
    }

}
