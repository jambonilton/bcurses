package org.nps.bcurses.data;

import java.util.Collections;
import java.util.Iterator;

public class LeafTextElement implements TextElement {

    final String text;
    final TextType type;

    public LeafTextElement(String text, TextType type) {
        this.text = text;
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public TextType getType() {
        return type;
    }

    @Override
    public boolean hasChildren() {
        return false;
    }

    @Override
    public Iterator<TextElement> iterator() {
        return Collections.emptyIterator();
    }

}
