package org.nps.bcurses.script;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class NashornScriptExecutor implements ScriptExecutor {

    public static final String ENGINE_NAME = "nashorn";

    final ScriptEngine base;

    public NashornScriptExecutor() {
        this(new ScriptEngineManager().getEngineByName(ENGINE_NAME));
    }

    private NashornScriptExecutor(ScriptEngine base) {
        this.base = base;
    }

    @Override
    public Object execute(String script) {
        try {
            return base.eval(script);
        } catch (ScriptException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void assign(String variable, Object value) {
        try {
            base.eval(String.format("%s='%s';", variable, value));
        } catch (ScriptException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Object invoke(String function, Object... args) {
        try {
            return ((Invocable) base).invokeFunction(function, args);
        } catch (NoSuchMethodException | ScriptException e) {
            throw new RuntimeException(e);
        }
    }

}
