package org.nps.bcurses.script;

public interface ScriptExecutor {
    Object execute(String script);
    void assign(String variable, Object value);
    Object invoke(String function, Object... args);
}
