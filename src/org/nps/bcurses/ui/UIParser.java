package org.nps.bcurses.ui;

import org.nps.bcurses.io.CharIterator;
import org.nps.bcurses.io.TextInput;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;

public interface UIParser {

    default UINode read(File file) {
        try (final FileReader reader = new FileReader(file)) {
            return read(new CharIterator(reader));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    default UINode read(String str) {
        return read(new CharIterator(new StringReader(str)));
    }

    UINode read(TextInput str);

}
