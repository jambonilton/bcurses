package org.nps.bcurses.ui;

public interface UIOption {

    String getScript();

}
