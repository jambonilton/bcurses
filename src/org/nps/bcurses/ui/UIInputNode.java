package org.nps.bcurses.ui;

import org.nps.bcurses.data.TextBuilder;
import org.nps.bcurses.data.TextElement;
import org.nps.bcurses.data.TextType;

import java.util.Collections;
import java.util.List;

public class UIInputNode implements UINode {

    final String variable;

    public UIInputNode(String variable) {
        this.variable = variable;
    }

    @Override
    public TextElement text() {
        return new TextBuilder().add("Enter value for ").add(variable, TextType.EMPHASIZED).add(": ").build();
    }

    @Override
    public List<UIOption> options() {
        return Collections.emptyList();
    }

    public String getVariable() {
        return variable;
    }
}
