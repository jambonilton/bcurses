package org.nps.bcurses.ui;

public interface UIState {

    void push(UINode node);

    UINode pop();

    boolean isEmpty();

    UIState set(String variable, Object value);

    Object get(String variable);

    UINode getRoot();

    void setRoot(UINode root);
}
