package org.nps.bcurses.ui;

import org.nps.bcurses.data.TextElement;

import java.util.List;

public interface UINode {

    TextElement text();

    List<UIOption> options();

    default boolean hasOptions() {
        return !options().isEmpty();
    }

}
