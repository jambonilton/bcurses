package org.nps.bcurses.ui;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class BaseUIState implements UIState {

    final Stack<UINode> nodeStack;
    final Map<String, Object> variables;

    UINode root;

    public BaseUIState() {
        this(new Stack<>(), new HashMap<>());
    }

    public BaseUIState(Stack<UINode> nodeStack, Map<String, Object> variables) {
        this.nodeStack = nodeStack;
        this.variables = variables;
    }

    @Override
    public void push(UINode node) {
        nodeStack.push(node);
    }

    @Override
    public UINode pop() {
        return nodeStack.pop();
    }

    @Override
    public boolean isEmpty() {
        return nodeStack.isEmpty();
    }

    @Override
    public UIState set(String variable, Object value) {
        variables.put(variable, value);
        return this;
    }

    @Override
    public Object get(String variable) {
        return variables.get(variable);
    }

    @Override
    public UINode getRoot() {
        return root;
    }

    @Override
    public void setRoot(UINode root) {
        this.root = root;
    }
}
