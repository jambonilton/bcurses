package org.nps.bcurses.ui;

import org.nps.bcurses.data.LinkElement;
import org.nps.bcurses.data.TextElement;

import java.util.ArrayList;
import java.util.List;

public class TextUINode implements UINode {

    final TextElement element;

    public TextUINode(TextElement element) {
        this.element = element;
    }

    @Override
    public TextElement text() {
        return element;
    }

    @Override
    public List<UIOption> options() {
        final List<UIOption> options = new ArrayList<>();
        addOptions(element, options);
        return options;
    }

    private void addOptions(TextElement element, List<UIOption> options) {
        if (!element.hasChildren()) {
            if (element instanceof LinkElement)
                options.add((LinkElement) element);
        } else {
            for (TextElement elem : element)
                addOptions(elem, options);
        }
    }


}
