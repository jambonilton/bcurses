package org.nps.bcurses.terminal;

import jline.ConsoleReader;
import org.nps.bcurses.data.TextElement;

import java.io.IOException;
import java.io.PrintStream;

public class TerminalImpl implements Terminal {

    final PrintStream out;
    final jline.ConsoleReader consoleReader;
    final TextSerializer textSerializer;

    public TerminalImpl() throws IOException {
        this(System.out, new jline.ConsoleReader(), new AnsiTextSerializer());
    }

    public TerminalImpl(PrintStream out, ConsoleReader consoleReader, TextSerializer textSerializer) {
        this.out = out;
        this.consoleReader = consoleReader;
        this.textSerializer = textSerializer;
    }

    @Override
    public void print(TextElement text) {
        print(textSerializer.apply(text));
    }

    @Override
    public void print(String msg) {
        out.print(msg);
    }

    @Override
    public void println(TextElement text) {
        println(textSerializer.apply(text));
    }

    @Override
    public void println(String msg) {
        out.println(msg);
    }

    @Override
    public int readKey() {
        try {
            return consoleReader.readVirtualKey();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String readLine() {
        try {
            return consoleReader.readLine();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
