package org.nps.bcurses.terminal;

import org.nps.bcurses.data.TextElement;

import java.util.function.Function;

public interface TextSerializer extends Function<TextElement, String> {

}
