package org.nps.bcurses.terminal;

import org.nps.bcurses.data.TextElement;

public interface Terminal {

    void println(String str);

    void println(TextElement text);

    void print(String str);

    void print(TextElement text);

    int readKey();

    String readLine();
}
