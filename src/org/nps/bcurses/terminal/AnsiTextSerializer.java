package org.nps.bcurses.terminal;

import org.nps.bcurses.data.TextElement;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;

import static org.nps.bcurses.data.TextType.*;

public class AnsiTextSerializer implements TextSerializer {

    private static final String ESCAPE = "\u001B[",
                                M = "m",
                                END = "0"+M;

    @Override
    public String apply(TextElement elem) {
        return apply(new AtomicInteger(), elem);
    }

    private String apply(AtomicInteger linkIndex, TextElement elem) {
        final StringBuilder sb = new StringBuilder();
        if (elem.is(PARAGRAPH) || elem.is(ROOT))
            sb.append("\n");
        final String text = elem.getText();
        if (elem.is(EMPHASIZED))
            sb.append(coloured(text + apply(linkIndex, elem.iterator()), Colour.RED));
        else if (elem.is(LINK))
            sb.append(coloured("("+linkIndex.incrementAndGet()+") ", Colour.RED))
                    .append(text).append(apply(linkIndex, elem.iterator()));
        else
            sb.append(text + apply(linkIndex, elem.iterator()));
        return sb.toString();
    }

    private String coloured(String text, Colour colour) {
        return ESCAPE + colour.fgBright() + M + text + ESCAPE + END;
    }

    private String apply(AtomicInteger linkIndex, Iterator<TextElement> elements) {
        final StringBuilder sb = new StringBuilder();
        while (elements.hasNext())
            sb.append(apply(linkIndex, elements.next()));
        return sb.toString();
    }

    public enum Colour {

        BLACK,
        RED,
        GREEN,
        YELLOW,
        BLUE,
        MAGENTA,
        CYAN,
        WHITE,
        DEFAULT;

        public int value()
        {
            return ordinal();
        }

        public int fg()
        {
            return ordinal() + 30;
        }

        public int bg()
        {
            return ordinal() + 40;
        }

        public int fgBright()
        {
            return ordinal() + 90;
        }

        public int bgBright()
        {
            return ordinal() + 100;
        }

    }

}
