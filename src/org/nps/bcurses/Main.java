package org.nps.bcurses;

import org.nps.bcurses.app.AppController;
import org.nps.bcurses.app.TerminalAppController;

import java.io.File;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        final AppController app = new TerminalAppController();
        final File file = validate(app, args);
        app.execute(file);
    }

    private static File validate(AppController app, String[] args) {
        if (args.length != 1)
            app.exit("Usage: bcurses <file>");
        final File file = new File(args[0]);
        if (!file.exists())
            app.exit("File \""+file.getName()+"\" does not exist!");
        return file;
    }
}
