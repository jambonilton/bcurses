package org.nps.bcurses.app;

import java.io.File;
import java.io.IOException;

public interface AppController {

    void exit(String message);

    void execute(File root) throws IOException;

}
