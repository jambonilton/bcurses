package org.nps.bcurses.app;

import org.nps.bcurses.parsing.UIParsers;
import org.nps.bcurses.script.NashornScriptExecutor;
import org.nps.bcurses.script.ScriptExecutor;
import org.nps.bcurses.terminal.Terminal;
import org.nps.bcurses.terminal.TerminalImpl;
import org.nps.bcurses.ui.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public class TerminalAppController implements AppController {

    private static final int BACKSPACE = 8,
                             ESCAPE = 27,
                             QUIT = 'q';

    public static final UINode DONE = null;

    final Terminal terminal;

    public TerminalAppController() throws IOException {
        this(new TerminalImpl());
    }

    public TerminalAppController(Terminal terminal) {
        this.terminal = terminal;
    }

    @Override
    public void exit(String message) {
        terminal.println("\n"+message+"\n");
        System.exit(0);
    }

    @Override
    public void execute(File file) throws IOException {
        new TerminalAppThread(file).run();
    }

    class TerminalAppThread implements Runnable {

        final UIParser nodeReader;
        final Path path;
        final ScriptExecutor scriptRunner;
        final UIState state;

        public TerminalAppThread(File file) {
            scriptRunner = new NashornScriptExecutor();
            nodeReader = UIParsers.markdownParserWithScripting(scriptRunner);
            path = file.getParentFile().toPath();
            state = new BaseUIState();
            state.setRoot(nodeReader.read(file));
        }

        @Override
        public void run() {
            UINode node = state.getRoot();
            while (node != DONE)
                node = getNextState(node);
            terminal.print("Goodbye!");
        }

        private UINode getNextState(UINode node) {

            terminal.print(node.text());

            if (node instanceof UIInputNode) {
                final String variable = ((UIInputNode) node).getVariable();
                final String input = terminal.readLine();
                scriptRunner.assign(variable, input);
                return state.pop();
            }

            final List<UIOption> options = node.options();
            if (options.isEmpty()) {
                if (state.isEmpty())
                    return DONE;
                return state.pop();
            } else {
                final int key = terminal.readKey();
                if (key == BACKSPACE || key == ESCAPE) {
                    if (state.isEmpty())
                        return DONE;
                    return state.pop();
                }
                else if (key == QUIT)
                    return DONE;
                else {
                    final int index = key - '1';
                    if (index < 0 || index >= options.size()) {
                        terminal.println("Invalid option: " + (char) key);
                        return node;
                    } else {
                        final UIOption option = node.options().get(index);
                        final String script = option.getScript();
                        final UINode next = resolve(script);
                        if (next == null || next.equals(node))
                            return node;
                        state.push(node);
                        return next;
                    }
                }
            }
        }

        private UINode resolve(String link) {
            if (link.startsWith("input:"))
                return resolveInputLink(link);
            return resolveFileLink(link);
        }

        private UINode resolveInputLink(String link) {
            final String variableName = link.split(":")[1];
            return new UIInputNode(variableName);
        }

        private UINode resolveFileLink(String link) {
            final Path resolved = path.resolve(link);
            return nodeReader.read(resolved.toFile());
        }

    }

}
