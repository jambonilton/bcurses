package org.nps.bcurses.io;

import com.google.common.collect.PeekingIterator;

public interface TextInput extends PeekingIterator<Character> {

    boolean isNext(CharSequence str);

    default void skip(CharSequence str) {
        for (int i=0; i < str.length(); i++)
            if (str.charAt(i) != next())
                throw new IllegalArgumentException("Unexpected character!");
    }

}
