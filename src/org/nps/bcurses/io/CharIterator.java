package org.nps.bcurses.io;

import java.io.IOException;
import java.io.Reader;
import java.nio.CharBuffer;

/**
 * Wrapper for reader implementation that allows for peeking ahead and managing other stately things.
 */
public class CharIterator implements TextInput {

    final Reader reader;

    private CharBuffer buffer;

    public CharIterator(Reader reader) {
        this(reader, 256);
    }

    public CharIterator(Reader reader, int bufferSize) {
        this.reader = reader;
        this.buffer = CharBuffer.allocate(bufferSize);
        buffer.limit(0);
    }

    @Override
    public Character peek() {
        return buffer.get(buffer.position());
    }

    @Override
    public Character next() {
        return buffer.get();
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean hasNext() {
        return buffer.hasRemaining() || readBuffer();
    }

    public boolean isNext(CharSequence str) {
        if (str.length() > buffer.capacity())
            throw new IllegalArgumentException("String "+str+" is too long for buffer.");
        else if (buffer.position() + str.length() >= buffer.limit() && !readBuffer())
            return false;
        for (int i=0; i < str.length(); i++)
            if (str.charAt(i) != get(i))
                return false;
        return true;
    }

    private char get(int i) {
        return buffer.get(buffer.position()+i);
    }

    private boolean readBuffer() {
        try {
            buffer.compact();
            if (reader.read(buffer) == -1) {
                buffer.limit(buffer.position());
                buffer.position(0);
                return false;
            }
            buffer.flip();
            return true;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
