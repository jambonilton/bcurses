B-Curses
========

This project is an attempt simplifying the process of user interface design.  Every node in a B-Curses UI follows a 
simple preamble-input layout, and collectively, the nodes form a tree structure that can be rendered into whatever sort 
of UI you want to make.

B-Curses uses a collection of extended markdown files for building a UI - the extensions include interactive forms and 
some scripting.  The interactive form elements fall under markdown links.

To view a sample application, see the files under /sample.