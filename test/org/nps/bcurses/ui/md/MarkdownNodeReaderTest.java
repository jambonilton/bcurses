package org.nps.bcurses.ui.md;

import org.junit.Before;
import org.junit.Test;
import org.nps.bcurses.data.TextElement;
import org.nps.bcurses.parsing.UIParsers;
import org.nps.bcurses.script.NashornScriptExecutor;
import org.nps.bcurses.terminal.TerminalImpl;
import org.nps.bcurses.ui.UINode;
import org.nps.bcurses.ui.UIParser;

import java.io.File;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class MarkdownNodeReaderTest {

    private final NashornScriptExecutor scriptExecutor = new NashornScriptExecutor();

    final UIParser reader = UIParsers.markdownParserWithScripting(scriptExecutor);

    @Before
    public void setUp() {
        scriptExecutor.assign("name", "Steve");
        scriptExecutor.assign("colour", "blue");
        scriptExecutor.assign("quest", "to seek the holy grail");
    }

    @Test
    public void read() throws Exception {
        final UINode node = reader.read("This is a *test*!");
        assertNotNull(node);
        final TextElement text = node.text();
        new TerminalImpl().print(text);
    }

    @Test
    public void readSample() throws Exception {
        final UINode node = reader.read(new File("sample/main.md"));
        assertNotNull(node);
        assertTrue(node.hasOptions());
        final TextElement text = node.text();
        new TerminalImpl().print(text);
    }


    @Test
    public void readRefs() throws Exception {
        final UINode node = reader.read(new File("sample/answered.md"));
        assertNotNull(node);
        final TextElement text = node.text();
        new TerminalImpl().print(text);
    }


    @Test
    public void readScript() throws Exception {
        final UINode node = reader.read("Count to ten: ${var sum=0; for (i=0;i<10;i++) sum+=i; sum}!  Woot!");
        assertNotNull(node);
        final TextElement text = node.text();
        new TerminalImpl().print(text);
    }


}