package org.nps.bcurses.terminal;

import org.junit.Test;
import org.nps.bcurses.data.TextElement;
import org.nps.bcurses.data.TextType;
import org.nps.bcurses.data.TextBuilder;

public class AnsiTextSerializerTest {

    AnsiTextSerializer serializer = new AnsiTextSerializer();

    @Test
    public void formatsEmphasis() throws Exception {
        final TextElement text = new TextBuilder()
                .add("This is a ")
                .add("test", TextType.EMPHASIZED)
                .add("!")
                .build();
        System.out.println(serializer.apply(text));
    }
}