In the distance you see a bear.

- [Play dead.](play_dead.md)
- [Attack!](attack_bear.md)
- [Offer the bear a promising career in software development.](job_offer.md)
