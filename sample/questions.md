Answer me these questions three, ere the other side ye see!

- [What is your name?](input:name)
- [What is your quest?](input:quest)
- [What is your favourite colour?](input:colour)
- [I've answered your questions, now let me pass!](answered.md)
