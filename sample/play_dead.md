As it turns out, this bear was smarter than average.  Your last moments in life are lived out failing to play dead 
as your insides are gobbled up by the bear.