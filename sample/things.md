Here's a list of things:
${
    var things = ['one', 'two', 'three'];
    var result = '';
    for each (var t in things)
        result += '\n - '+t;
    result;
}

Those were the things!